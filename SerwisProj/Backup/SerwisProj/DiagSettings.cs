﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace SerwisProj
{
    public class DiagSettings
    {
        private static DiagSettings instance = null;
        public List<ButtonAdv> buttonList = null;
        public List<Label> labelList = null;
        public Dictionary<String,String> stringList = null;

        private DiagSettings()
        {
            buttonList = new List<ButtonAdv>();
            labelList = new List<Label>();
            stringList = new Dictionary<String,String>();
        }

        public static DiagSettings getSetting()
        {
            if (instance == null)
            {
                instance = new DiagSettings();
                return instance;
            }
            return instance;

        }
        public void about()
        {
            Console.WriteLine("Identyfiaktor = ");
        }
    }
}