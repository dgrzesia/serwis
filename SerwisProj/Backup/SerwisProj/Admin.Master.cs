﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            setDb();
            Response.Redirect("Adm_producent.aspx", false);
        }


        protected void Button2_Click(object sender, EventArgs e)
        {
            setDb();
            Response.Redirect("Adm_Produkt.aspx", false);
        }
        protected void Button3_Click(object sender, EventArgs e)
        {
            setDb();
            Response.Redirect("Adm_Zamowienia.aspx", false);
        }
        protected void Button4_Click(object sender, EventArgs e)
        {
            setDb();
            Response.Redirect("Adm_usery.aspx", false);
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            setDb();
            Response.Redirect("Adm_Msg.aspx", false);
        }

        protected void setDb()
        {
            string insertSql = "UPDATE [Serwis].[dbo].[zamowienia] SET viewed = 1";
            SqlConnection myConnection = new SqlConnection(this.ConnectionString);
            myConnection.Open();
            SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
            myCommand.ExecuteNonQuery();
            myConnection.Close();
        }
    }
}