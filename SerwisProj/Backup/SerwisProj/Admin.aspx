﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin.master" CodeBehind="Admin.aspx.cs" Inherits="SerwisProj.Admin1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContentPlaceHolder1" Runat="Server">
    <h1>Panel Administracyjny</h1>

    <h2>
        Ostatnie zamówienia    
    </h2>
    <p>
        <asp:GridView ID="order_view" runat="server" 
            DataSourceID="SqlDataSource1">
            <Columns>

            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SerwisConnectionString %>" 
            SelectCommand=" SELECT usr.Nazwa as Urzytkownik, usl.nazwa as Zlecenie, zam.data as Data
                            FROM [Serwis].[dbo].[zamowienia] zam
                            LEFT JOIN [Serwis].[dbo].[usery] usr 
	                            ON usr.id_user = zam.id_user
                            LEFT JOIN [Serwis].[dbo].[Uslugi] usl
	                            ON usl.ID_uslugi  = zam.id_uslugi
                            WHERE zam.viewed = 0">
        </asp:SqlDataSource>
    </p>
    <p>
        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
            Text="Wyloguj się" />
    </p>
</asp:Content>
