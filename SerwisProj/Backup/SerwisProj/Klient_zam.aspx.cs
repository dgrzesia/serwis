﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class Klient_zam : System.Web.UI.Page
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;
        public int Client_ID;

        protected String Authenticate()
        {
            String tmp = "";
            object state;
            if (null == (state = HttpContext.Current.Session["id_user"]))
            {
                this.ToLogin();
            }
            Client_ID = Int32.Parse(state.ToString());

            //int ID = Int16.Parse(((Button)state).ID);


            string insertSql = "select id_user from usery where id_user = @id_user";

            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();

                SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                myCommand.Parameters.AddWithValue("@id_user", this.Client_ID);
                object sklep = myCommand.ExecuteScalar();
                tmp = sklep.ToString();
                myConnection.Close();

            }
            return tmp;
        }

        protected void ToLogin()
        {
            Response.Redirect("Default.aspx");
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            String tmp = this.Authenticate();


            using (SqlConnection sqlConnection = new SqlConnection(this.ConnectionString))
            {
                string sqlQuery = @"SELECT  us .Nazwa as usNazwa, z.data as zData, z.zaliczka as zZaliczka, us.cena as usCena, z.id_user as zID
  FROM zamowienia as z
	left join uslugi as us on (z.id_uslugi = us.ID_uslugi)
		order by z.data";
                using (SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection))
                {
                    sqlConnection.Open();
                    using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
                    {
                        Table1.Width = 600;
                        using (TableRow tableRow = new TableRow())
                        {
                            Table1.Rows.Add(tableRow);
                            tableRow.Cells.Add(new TableCell() {/* Text = "Uslugi",*/ ColumnSpan = 3, VerticalAlign = VerticalAlign.Middle, HorizontalAlign = HorizontalAlign.Center });
                        }
                        while (sqlReader.Read())
                        {


                            using (TableRow tableRow = new TableRow())
                            {
                                {
                                    if (tmp == sqlReader["zID"].ToString())
                                    {
                                        Table1.Rows.Add(tableRow);
                                        tableRow.Cells.Add(new TableCell() { Text = sqlReader["usNazwa"].ToString() });
                                        tableRow.Cells.Add(new TableCell() { Text = sqlReader["zData"].ToString() });
                                        tableRow.Cells.Add(new TableCell() { Text = sqlReader["usCena"].ToString() });
                                        // tableRow.Cells.Add(new TableCell() { Text = sqlReader["pZaliczka"].ToString() });

                                        TableCell tc = new TableCell();
                                        tableRow.Cells.Add(tc);
                                    }
                                }
                            }
                        }
                        sqlReader.Close();
                    }

                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            ToLogin();
        }


    }
}