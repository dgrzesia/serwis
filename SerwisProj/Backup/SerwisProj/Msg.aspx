﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Msg.aspx.cs" Inherits="SerwisProj.Msg" MasterPageFile="~/MasterPage.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContentPlaceHolder1" Runat="Server">

    <p>
        <asp:Label ID="msg_label" runat="server" Text="Wprowadz swój problem"></asp:Label>
    </p>
    <p>   
        <asp:TextBox ID="msgs_box" runat="server" Width="600px"></asp:TextBox>    
    </p>
    <p>        
        <asp:Button ID="send_btn" runat="server" Text="Wyślij" 
            onclick="send_btn_Click" />        
        <asp:Button ID="mainPageBtn" runat="server" Text="Strona główna" 
            Visible="False" onclick="mainPageBtn_Click"/>
    </p>
</asp:Content>