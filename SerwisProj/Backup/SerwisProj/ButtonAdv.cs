﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SerwisProj
{
    public class ButtonAdv : System.Web.UI.WebControls.Button
    {
        public bool flag;
        public int questionId;
        public int nextId;
        public bool isPressed = false;
        public ButtonAdv(Boolean flag, int qId, Boolean enabled, int nextId)
        {
            this.flag = flag;
            this.nextId = nextId;
            questionId = qId;
            isPressed = enabled;
            if (flag)
            {
                this.Text = "Tak"; ;
            }
            else
            {
                this.Text = "Nie";
            }

            if (enabled)
            {
                this.Enabled = true;
            }
            else
            {
                this.Enabled = false;
            }
        }
    }
}