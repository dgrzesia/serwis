﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class Register : System.Web.UI.Page
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button6_Click(object sender, EventArgs e)
        {
            {
                string insertSql = "insert into usery(nazwa, imie, nazwisko, login, haslo, id_typ, email) OUTPUT (inserted.id_user) into adres(id_u) values(@nazwa, @imie, @nazwisko, @login, @haslo,@id_typ, @email)";

                using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
                {
                    myConnection.Open();

                    SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                    myCommand.Parameters.AddWithValue("@nazwa", TextBox1.Text);
                    myCommand.Parameters.AddWithValue("@imie", TextBox3.Text);
                    myCommand.Parameters.AddWithValue("@nazwisko", TextBox2.Text);
                    myCommand.Parameters.AddWithValue("@login", TextBox5.Text);
                    myCommand.Parameters.AddWithValue("@haslo", TextBox6.Text);
                    myCommand.Parameters.AddWithValue("@id_typ", "1");
                    myCommand.Parameters.AddWithValue("@email", TextBox7.Text);

                    myCommand.ExecuteNonQuery();
                    myConnection.Close();

                }

                string insertSql2 = @"UPDATE adres SET ulica = @ulica, nr=@nr, 
                                                miasto = @miasto, państwo = @panstwo
                                      WHERE id_u=(select MAX(id_user) from usery)";

                using (SqlConnection myConnection2 = new SqlConnection(this.ConnectionString))
                {
                    myConnection2.Open();

                    SqlCommand myCommand2 = new SqlCommand(insertSql2, myConnection2);
                    myCommand2.Parameters.AddWithValue("@ulica", TextBox4.Text);
                    myCommand2.Parameters.AddWithValue("@nr", TextBox8.Text);
                    myCommand2.Parameters.AddWithValue("@miasto", TextBox10.Text);
                    myCommand2.Parameters.AddWithValue("@panstwo", TextBox9.Text);
                    //myCommand2.Parameters.AddWithValue("@id_user", );
                    //ParameterDirection.Output

                    myCommand2.ExecuteNonQuery();
                    myConnection2.Close();

                }
                //Response.Redirect(Request.RawUrl);

                String page = "";
                page = "Default.aspx";
                Response.Redirect(page);
            }
        }
    }
}