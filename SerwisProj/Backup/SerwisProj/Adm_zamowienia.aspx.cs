﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class Adm_zamowienia : System.Web.UI.Page
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;
        public int Client_ID;

        protected void Authenticate()
        {
            object state;
            if (null == (state = HttpContext.Current.Session["id_user"]))
            {
                this.ToLogin();
            }
            Client_ID = Int32.Parse(state.ToString());
        }

        protected void ToLogin()
        {
            Response.Redirect("Default.aspx");
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Authenticate();
            using (SqlConnection sqlConnection = new SqlConnection(this.ConnectionString))
            {
                string sqlQuery = @"SELECT zamowienia.id_zlecenia as idz, zamowienia.id_user as idur, 
		                                zamowienia.id_uslugi as idu, zamowienia.data as date,
                                        uslugi.nazwa as uNazwa, usery.nazwa as urNazwa
                                FROM zamowienia
                                        left join uslugi on uslugi.id_uslugi = zamowienia.id_uslugi
                                        left join usery on usery.id_user = zamowienia.id_user
                                order by urNazwa, uNazwa";
                using (SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection))
                {
                    sqlConnection.Open();
                    using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
                    {
                        Table1.Width = 600;
                        using (TableRow tableRow = new TableRow())
                        {
                            Table1.Rows.Add(tableRow);
                            tableRow.Cells.Add(new TableCell() { ColumnSpan = 5, VerticalAlign = VerticalAlign.Middle, HorizontalAlign = HorizontalAlign.Center });
                        }
                        while (sqlReader.Read())
                        {
                            using (TableRow tableRow = new TableRow())
                            {
                                Table1.Rows.Add(tableRow);
                                Button removeButton = new Button { Text = "Wykonane", ID = sqlReader["idz"].ToString() };
                                tableRow.Cells.Add(new TableCell() { Text = sqlReader["urNazwa"].ToString() });
                                tableRow.Cells.Add(new TableCell() { Text = sqlReader["uNazwa"].ToString() });
                                //tableRow.Cells.Add(new TableCell() { Text = sqlReader["idu"].ToString() });
                                tableRow.Cells.Add(new TableCell() { Text = sqlReader["date"].ToString() });

                                removeButton.Click += new EventHandler(btn_click);
                                TableCell tc = new TableCell();
                                tc.Controls.Add(removeButton);
                                tableRow.Cells.Add(tc);
                            }
                        }
                        sqlReader.Close();
                    }
                }
            }
        }


        public override void VerifyRenderingInServerForm(Control control) { }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            ToLogin();
        }

        public void btn_click(object sender, EventArgs e)
        {
            int ID = Int16.Parse(((Button)sender).ID);


            string insertSql = "DELETE FROM zamowienia WHERE id_zlecenia=@idz;";

            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();

                SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                myCommand.Parameters.AddWithValue("@idz", ID);
                object sklep = myCommand.ExecuteScalar();

                myCommand.ExecuteNonQuery();
                myConnection.Close();

            }
            Response.Redirect(Request.RawUrl);
        }
    }
}