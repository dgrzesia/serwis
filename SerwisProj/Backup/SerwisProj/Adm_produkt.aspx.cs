﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class Adm_produkty : System.Web.UI.Page
    {
        private int Worker_ID;
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;

        protected void Authenticate()
        {
            object state;
            if (null == (state = HttpContext.Current.Session["id_user"]))
            {
                this.ToLogin();
            }
            Worker_ID = Int32.Parse(state.ToString());
        }

        protected void ToLogin()
        {
            Response.Redirect("Default.aspx");
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Authenticate();

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            ToLogin();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string insertSql = "insert into Uslugi(Nazwa, cena) values(@nazwa, @cena)";

            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();

                SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                myCommand.Parameters.AddWithValue("@nazwa", TextBox1.Text);
                //myCommand.Parameters.AddWithValue("@ID_P", DropDownList1.SelectedValue);
                myCommand.Parameters.AddWithValue("@cena", TextBox2.Text);
                myCommand.ExecuteNonQuery();
                myConnection.Close();

            }
            Response.Redirect(Request.RawUrl);
        }
        protected void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}