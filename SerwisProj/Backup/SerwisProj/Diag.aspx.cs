﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class Diag : System.Web.UI.Page
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;
        String Opis, Nazwa, Cena, rozwiazanie;
        public ContentPlaceHolder contentPlaceHolder;
        protected void Page_Load(object sender, EventArgs e)
        {
            contentPlaceHolder = Page.Master.FindControl("MenuContentPlaceHolder1") as ContentPlaceHolder;
            Label msglabel = new Label();
            msglabel.Text = "Nie znalazles rozwiazania? Wyslij nam opis swojgo problemu";
            contentPlaceHolder.Controls.Add(msglabel);
            Button but = new Button();
            but.Text = "Wyślij wiadomość";
            but.Click += new EventHandler(msg_click);
            contentPlaceHolder.Controls.Add(but); 
        }

        protected void msg_click(object sender, EventArgs e)
        {
            String page = "";
            page = "Msg.aspx";
            Response.Redirect(page);
        }

        protected void Ukrywanie(int poziom)
        {
            switch (poziom)
            {
                case 1:
                    {
                        VisibleFalse.Visible = false;
                        VisibleTrue.Visible = false;
                        Label1.Visible = false;
                        Ukrywanie(2);
                        TurnOnTrue.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        TurnOnFalse.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        break;
                    }
                case 2:
                    {
                        BipPanel.Visible = false;
                        BiosMsg.Visible = false;
                        SystemLoadingPanel.Visible = false;
                        VisibleTrue.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        VisibleFalse.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Label2.Visible = false;
                        Ukrywanie(3);
                        break;
                    }
                case 3:
                    {
                        Label3.Visible = false;
                        Label5.Visible = false;
                        SpeakerBlack.Visible = false;
                        SystemLoadingPanel.Visible = false;
                        BipFalse.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        BipTrue.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Disc.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        NTLDR.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        F1.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Zaden.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Ukrywanie(4);
                        break;
                    }
                case 4:
                    {
                        Label4.Visible = false;
                        SystemLoading.Visible = false;
                        SystemLoadingTrue.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        SystemLoadingFalse.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        SpeakerBlackTrue.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        SpeakerBlackFalse.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Ukrywanie(5);
                        break;
                    }
                case 5:
                    {
                        Label6.Visible = false;
                        WithoutError.Visible = false;
                        SystemNotLoaded.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        SystemLoaded.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Ukrywanie(6);
                        break;
                    }
                case 6:
                    {
                        Label7.Visible = false;
                        ResetedPanel.Visible = false;
                        LoginErrorPanel.Visible = false;
                        woErrorF.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        woErrorT.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Ukrywanie(7);
                        break;
                    }
                case 7:
                    {
                        Label8.Visible = false;
                        ResetedF.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        ResetedT.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        LoginError.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        LoginNotError.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        DesktopPanel.Visible = false;
                        PasswdPanel.Visible = false;
                        Ukrywanie(8);
                        break;
                    }
                case 8:
                    {
                        Label10.Visible = false;
                        LoadUserPanel.Visible = false;
                        DeskktopF.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        DesktopT.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        PassF.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        PassOk.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Ukrywanie(9);
                        break;
                    }
                case 9:
                    {
                        Label9.Visible = false;
                        DesktopPanel2.Visible = false;
                        LoadT.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        LoadF.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Ukrywanie(10);
                        break;
                    }
                case 10:
                    {
                        Label11.Visible = false;
                        BlackPx.Visible = false;
                        Resolution.Visible = false;
                        DesktopF3.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        DesktopT2.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Ukrywanie(11);
                        break;
                    }
                case 11:
                    {
                        Label12.Visible = false;
                        UpdatePanel.Visible = false;
                        MultiBlurPanel.Visible = false;
                        BlackPxF.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        BlackPxT.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        ResYes.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        ResNo.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Ukrywanie(12);
                        break;
                    }
                case 12:
                    {
                        Label13.Visible = false;
                        AfterErrorPanel.Visible = false;
                        HowTimePanel.Visible = false;
                        UpdateY.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        UpdateN.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        MultiN.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        MultiY.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Ukrywanie(13);
                        break;
                    }
                case 13:
                    {
                        Label14.Visible = false;
                        BsodPanel.Visible = false;
                        AfterY.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        AfterN.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        TimeN.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        TimeY.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        Ukrywanie(14);
                        break;
                    }
                case 14:
                    {
                        Label15.Visible = false;
                        BsodN.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        BsodY.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        ApplicationPanel.Visible = false;
                        BsodoNrPanel.Visible = false;
                        Ukrywanie(15);
                        break;
                    }
                case 15:
                    {
                        Label16.Visible = false;
                        AppN.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        AppY.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                        break;
                    }
            }
        }

        protected void DesktopShow(bool x)
        {
            Label11.Visible = true;
            if (x)
            {
                Label11.Text = "Czy wyświetla się powiadomienie o niepasującej rozdzielczości ekranu?";

                Resolution.Visible = true;
            }
            else
            {
                Label11.Text = "Czy pojawiają się czarne kropki?";
                BlackPx.Visible = true;
            }
        }

        protected void execSql(String problemSql)
        {
            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();
                SqlCommand myCommand = new SqlCommand(problemSql, myConnection);
                using (var baza = myCommand.ExecuteReader())
                {
                    if (baza.Read())
                    {
                        Opis = baza["Opis"].ToString();
                        Nazwa = baza["Nazwa"].ToString();
                        Cena = baza["Cena"].ToString();
                    }
                }
                myConnection.Close();
            }
            //Response.Redirect(Request.RawUrl);
        }

        protected void SystemLoaded_CheckedChanged(object sender, EventArgs e)
        {

        }
        protected void TurnOnTrue_Click(object sender, EventArgs e)
        {
            Ukrywanie(1);
            TurnOnTrue.BackColor = System.Drawing.Color.Red;
            Label1.Visible = true;
            Label1.Text = "Czy pojawia sie coś na ekranie?";
            VisibleTrue.Visible = true;
            VisibleFalse.Visible = true;
        }
        protected void TurnOnFalse_Click(object sender, EventArgs e)
        {
            Ukrywanie(1);
            TurnOnFalse.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_sprzetowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=1";
            execSql(problemSql);
            Label1.Visible = true;
            Label1.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;


        }
        protected void VisibleTrue_Click(object sender, EventArgs e)
        {
            Ukrywanie(2);
            VisibleTrue.BackColor = System.Drawing.Color.Red;

            BiosMsg.Visible = true;
            Label2.Visible = true; ;
            Label2.Text = "Czy pojawia sie któryś z tych komunikatów?";
        }
        protected void VisibleFalse_Click(object sender, EventArgs e)
        {
            Ukrywanie(2);
            VisibleFalse.BackColor = System.Drawing.Color.Red;
            BipPanel.Visible = true;
            Label2.Visible = true;
            Label2.Text = "Czy slychac piszczenie/pikanie?";
            BipTrue.Text = "Tak";
            BipFalse.Text = "Nie";
        }
        protected void BipTrue_Click(object sender, EventArgs e)
        {
            Ukrywanie(3);
            BipTrue.BackColor = System.Drawing.Color.Red;
            Label3.Visible = true;
            Label3.Text = "Czy po sygnale speaker'a ekran jest czarny?";
            SpeakerBlack.Visible = true;

        }
        protected void BipFalse_Click(object sender, EventArgs e)
        {
            Ukrywanie(3);
            BipFalse.BackColor = System.Drawing.Color.Red;
            Label3.Visible = true; ;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_sprzetowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=6";
            execSql(problemSql);
            Label3.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void SystemLoadingTrue_Click(object sender, EventArgs e)
        {
            if (SpeakerBlack.Visible == true)
            {
                Ukrywanie(4);
                SpeakerBlackFalse.BackColor = System.Drawing.Color.Red;
            }
            else
                Ukrywanie(4);
            SystemLoadingTrue.BackColor = System.Drawing.Color.Red;
            SystemLoading.Visible = true;
            Label4.Visible = true;
            Label4.Text = "Czy system sie wczytał?";
        }
        protected void SystemLoadingFalse_Click(object sender, EventArgs e)
        {
            if (SpeakerBlack.Visible == true)
            {
                Ukrywanie(4);
                SpeakerBlackFalse.BackColor = System.Drawing.Color.Red;
            }
            else
                Ukrywanie(4);
            SystemLoadingFalse.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=4";
            execSql(problemSql);
            Label4.Visible = true;
            Label4.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void SystemNotLoaded_Click(object sender, EventArgs e)
        {
            Ukrywanie(5);
            SystemNotLoaded.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=5";
            execSql(problemSql);
            Label6.Visible = true;
            Label6.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void SystemLoaded_Click(object sender, EventArgs e)
        {
            Ukrywanie(5);
            SystemLoaded.BackColor = System.Drawing.Color.Red;
            Label6.Visible = true;
            Label6.Text = "Czy system wczytał się bez błędów?";
            WithoutError.Visible = true;
        }
        protected void Disc_Click(object sender, EventArgs e)
        {
            Ukrywanie(3);
            Disc.BackColor = System.Drawing.Color.Red;
            Label3.Visible = true; ;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_sprzetowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=7";
            execSql(problemSql);
            Label3.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void NTLDR_Click(object sender, EventArgs e)
        {
            Ukrywanie(3);
            NTLDR.BackColor = System.Drawing.Color.Red;
            Label3.Visible = true; ;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=3";
            execSql(problemSql);
            Label3.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void F1_Click(object sender, EventArgs e)
        {
            Ukrywanie(3);
            F1.BackColor = System.Drawing.Color.Red;
            Label3.Visible = true; ;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_sprzetowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=8";
            execSql(problemSql);
            Label3.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void Zaden_Click(object sender, EventArgs e)
        {
            Ukrywanie(3);
            Zaden.BackColor = System.Drawing.Color.Red;
            SystemLoadingPanel.Visible = true;
            Label3.Visible = true; ;
            Label3.Text = "Czy pojawia sie ekran wczytywania systemu?";
            SystemLoadingTrue.Text = "Tak";
            SystemLoadingFalse.Text = "Nie";
        }
        protected void SpeakerBlackFalse_Click(object sender, EventArgs e)
        {
            Ukrywanie(4);
            SpeakerBlackFalse.BackColor = System.Drawing.Color.Red;
            SystemLoadingPanel.Visible = true;
            Label5.Visible = true; ;
            Label5.Text = "Czy pojawia sie ekran wczytywania systemu?";
            SystemLoadingTrue.Text = "Tak";
            SystemLoadingFalse.Text = "Nie";
        }
        protected void SpeakerBlackTrue_Click(object sender, EventArgs e)
        {
            Ukrywanie(4);
            SystemLoadingPanel.Visible = false;
            Label5.Visible = false;
            SpeakerBlackTrue.BackColor = System.Drawing.Color.Red;
            Label4.Visible = true; ;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_sprzetowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=9";
            execSql(problemSql);
            Label4.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void woErrorF_Click(object sender, EventArgs e)
        {
            Ukrywanie(6);
            woErrorF.BackColor = System.Drawing.Color.Red;
            Label7.Visible = true;
            Label7.Text = "Czy system się zresetował?";
            ResetedPanel.Visible = true;
        }
        protected void woErrorT_Click(object sender, EventArgs e)
        {
            Ukrywanie(6);
            woErrorT.BackColor = System.Drawing.Color.Red;
            Label7.Visible = true;
            Label7.Text = "Czy wystąpiły problemy z logowaniem?";
            LoginErrorPanel.Visible = true;
        }
        protected void LoginError_Click(object sender, EventArgs e)
        {
            Ukrywanie(7);
            LoginError.BackColor = System.Drawing.Color.Red;
            Label8.Visible = true;
            Label8.Text = "Czy system nie akceptuje hasla?";
            PasswdPanel.Visible = true;
        }
        protected void LoginNotError_Click(object sender, EventArgs e)
        {
            Ukrywanie(7);
            LoginNotError.BackColor = System.Drawing.Color.Red;
            Label8.Visible = true;
            Label8.Text = "Czy pulpit poprawnie się wyświetla?";
            DesktopPanel.Visible = true;
        }
        protected void ResetedT_Click(object sender, EventArgs e)
        {
            /*Ukrywanie(7);
            ResetedT.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=6";
            execSql(problemSql);
            Label8.Visible = true;
            Label8.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;*/
            Ukrywanie(7);
            ResetedT.BackColor = System.Drawing.Color.Red;
            Label14.Text = "Czy pojawia sie blue screen - niebieski ekran?";
            Label14.Visible = true;
            BsodPanel.Visible = true;
            //AfterN.BackColor = System.Drawing.Color.Red;

        }
        protected void ResetedF_Click(object sender, EventArgs e)
        {
            Ukrywanie(7);
            ResetedF.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=5";
            execSql(problemSql);
            Label8.Visible = true;
            Label8.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void PassOk_Click(object sender, EventArgs e)
        {
            Ukrywanie(8);
            Label9.Visible = true;
            PassOk.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=6";
            execSql(problemSql);
            Label9.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void PassF_Click(object sender, EventArgs e)
        {
            Ukrywanie(8);
            PassF.BackColor = System.Drawing.Color.Red;
            Label10.Visible = true;
            Label10.Text = "Czy system nie chce wczytać prfilu użytkownika?";
            LoadUserPanel.Visible = true;
        }
        protected void DesktopT_Click(object sender, EventArgs e)
        {
            Ukrywanie(8);
            DesktopShow(true);
            DesktopT.BackColor = System.Drawing.Color.Red;
        }
        protected void DeskktopF_Click(object sender, EventArgs e)
        {
            Ukrywanie(8);
            DesktopShow(false);
            DeskktopF.BackColor = System.Drawing.Color.Red;
        }
        protected void LoadT_Click(object sender, EventArgs e)
        {
            Ukrywanie(9);
            Label9.Visible = true;
            LoadT.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=7";
            execSql(problemSql);
            Label9.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void LoadF_Click(object sender, EventArgs e)
        {
            Ukrywanie(9);
            DesktopPanel.Visible = false;
            LoadF.BackColor = System.Drawing.Color.Red;
            Label9.Visible = true;
            Label9.Text = "Czy pulpit poprawnie się wyświetla?";
            DesktopPanel2.Visible = true;
        }
        protected void DesktopT2_Click(object sender, EventArgs e)
        {
            Ukrywanie(10);
            DesktopShow(true);
            DesktopT2.BackColor = System.Drawing.Color.Red;
        }
        protected void DeskktopF2_Click(object sender, EventArgs e)
        {
            Ukrywanie(10);
            DesktopShow(false);
            DesktopF3.BackColor = System.Drawing.Color.Red;
        }
        protected void BlackPxF_Click(object sender, EventArgs e)
        {
            Ukrywanie(11);
            Label12.Visible = true;
            Label12.Text = "Czy pojawiają się wielokolorowe plamy?";
            MultiBlurPanel.Visible = true;
            BlackPxF.BackColor = System.Drawing.Color.Red;
        }
        protected void BlackPxT_Click(object sender, EventArgs e)
        {
            Ukrywanie(11);
            Label12.Visible = true;
            BlackPxT.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_sprzetowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=11";
            execSql(problemSql);
            Label12.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void ResNo_Click(object sender, EventArgs e)
        {
            Ukrywanie(11);
            Label12.Visible = true;
            Label12.Text = "Czy pojawia sie komunikat Aktualizacje automatyczne wyłączone?";
            UpdatePanel.Visible = true;
            ResNo.BackColor = System.Drawing.Color.Red;
        }
        protected void ResYes_Click(object sender, EventArgs e)
        {
            Ukrywanie(11);
            Label12.Visible = true;
            ResYes.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=9";
            execSql(problemSql);
            Label12.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void UpdateY_Click(object sender, EventArgs e)
        {
            Ukrywanie(12);
            Label13.Visible = true;
            UpdateY.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=10";
            execSql(problemSql);
            Label13.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void UpdateN_Click(object sender, EventArgs e)
        {
            Ukrywanie(12);
            Label13.Visible = true;
            Label13.Text = "Czy wyswietla sie komunikat System odzyskal sprawnosc po powaznym bledzie?";
            AfterErrorPanel.Visible = true;
            UpdateN.BackColor = System.Drawing.Color.Red;
        }
        protected void MultiN_Click(object sender, EventArgs e)
        {
            Ukrywanie(12);
            Label13.Visible = true;
            Label13.Text = "Czy po jakims czasie uzytkowania systemu, na ekranie wyswietlily sie wielokolorowe pasy?";
            HowTimePanel.Visible = true;
            MultiN.BackColor = System.Drawing.Color.Red;
        }
        protected void MultiY_Click(object sender, EventArgs e)
        {
            Ukrywanie(12);
            Label13.Visible = true;
            MultiY.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=8";
            execSql(problemSql);
            Label13.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void TimeN_Click(object sender, EventArgs e)
        {
            Ukrywanie(13);
            Label14.Visible = true;
            TimeN.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_sprzetowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=12";
            execSql(problemSql);
            Label14.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void TimeY_Click(object sender, EventArgs e)
        {
            Ukrywanie(13);
            Label14.Visible = true;
            TimeY.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=11";
            execSql(problemSql);
            Label14.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void AfterN_Click(object sender, EventArgs e)
        {
            Ukrywanie(13);
            Label14.Text = "Czy pojawia sie blue screen - niebieski ekran?";
            Label14.Visible = true;
            BsodPanel.Visible = true;
            AfterN.BackColor = System.Drawing.Color.Red;
        }
        protected void AfterY_Click(object sender, EventArgs e)
        {
            Ukrywanie(13);
            Label14.Visible = true;
            AfterY.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=13";
            execSql(problemSql);
            Label14.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void BsodN_Click(object sender, EventArgs e)
        {
            Ukrywanie(14);
            Label15.Text = "Błąd dotyczy programu/applikacji?";
            Label15.Visible = true;
            ApplicationPanel.Visible = true;
            BsodN.BackColor = System.Drawing.Color.Red;
        }
        protected void BsodY_Click(object sender, EventArgs e)
        {
            Ukrywanie(14);
            Label15.Text = "Wybież z listy nr błędu który pojawia się podczas blue screen'a";
            Label15.Visible = true;
            BsodoNrPanel.Visible = true;
            BsodY.BackColor = System.Drawing.Color.Red;
            Label16.Visible = true;
            string problemSql = "select bs.rozwiazanie, us.nazwa, us.cena " +
                                    " from bsod as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_uslugi " +
                                    " where bs.id=1";
            BsodList.SelectedValue = "1";
            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();
                SqlCommand myCommand = new SqlCommand(problemSql, myConnection);
                using (var baza = myCommand.ExecuteReader())
                {
                    if (baza.Read())
                    {
                        rozwiazanie = baza["rozwiazanie"].ToString();
                        Nazwa = baza["Nazwa"].ToString();
                        Cena = baza["Cena"].ToString();
                    }
                }
                myConnection.Close();
            }
            Label16.Text = rozwiazanie + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void AppY_Click(object sender, EventArgs e)
        {
            Ukrywanie(15);
            Label16.Visible = true;
            AppY.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_systemowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=14";
            execSql(problemSql);
            Label16.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void AppN_Click(object sender, EventArgs e)
        {
            Ukrywanie(15);
            Label16.Visible = true;
            AppN.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.opis, us.nazwa, us.cena " +
                                    " from baza_sprzetowa as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_u " +
                                    " where id_b=12";
            execSql(problemSql);
            Label16.Text = Opis + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
        protected void BsodList_SelectedIndexChanged(object sender, EventArgs e)
        {
            Ukrywanie(15);
            Label16.Visible = true;
            //AppN.BackColor = System.Drawing.Color.Red;
            string problemSql = "select bs.rozwiazanie, us.nazwa, us.cena " +
                                    " from bsod as bs " +
                                    " left join uslugi as us on us.id_uslugi = bs.id_uslugi " +
                                    " where bs.id=" + BsodList.SelectedValue;
            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();
                SqlCommand myCommand = new SqlCommand(problemSql, myConnection);
                using (var baza = myCommand.ExecuteReader())
                {
                    if (baza.Read())
                    {
                        rozwiazanie = baza["rozwiazanie"].ToString();
                        Nazwa = baza["Nazwa"].ToString();
                        Cena = baza["Cena"].ToString();
                    }
                }
                myConnection.Close();
            }
            Label16.Text = rozwiazanie + ". Proponowana usługa: " + Nazwa + ". Cena: " + Cena;
        }
    }
}