﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeBehind="Default.aspx.cs" Inherits="SerwisProj.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContentPlaceHolder1" Runat="Server">   
    <p>
        Doskonale wiemy o tym, że awaria komputera może narazić jego właściciela na poważne straty czasu i pieniędzy. 
    </p><p>
        Z tego powodu uruchomiliśmy pogotowie komputerowe, którego zadaniem jest jak najszybsza pomoc w przypadku awarii sprzętu komputerowego.
    </p><p>
        Jeśli potrzebujesz pomocy w rozwiązaniu problemów z Twoim sprzętem komputerowym, skontaktuj się z naszą firmą. 
    </p><p>
        Jadąc do klienta zabieramy ze sobą podstawowe podzespoły komputerowe – dzięki temu, w przypadku awarii sprzętowej od razu możemy rozwiązać problem.
    </p><p>
        W ramach naszych usług świadczymy:
    </p>
    <ul type = circle>
       <li> usuwanie wirusów</li> 
       <li> konfigurację drukarek i skanerów</li> 
       <li> odzyskiwanie utraconych danych</li> 
       <li> instalację systemów Windows, Linux</li> 
       <li> konfigurację routerów</li> 
       <li> instalację oprogramowania ( np. Pakiet Office)</li> 
       <li> konfigurację poczty e-mail</li> 
       <li> konfigurację sieci bezprzewodowych WiFi</li> 
       <li> rozwiązywanie problemów z Internetem</li> 
       <li> instalowanie Neostrady, DSL</li> 
    </ul>

    <p>Możesz także spróbować określić problem</p>
    <p>oraz proponowaną cene rozwiązania</p>
    <p>za pomocą naszego systemu
    </p>
    <h2><asp:Button ID="Button3" runat="server" Text="Diagnostyka" 
            onclick="Button3_Click" /></h2>

    <p style="text-align: center">
    <asp:Button ID="Button1" runat="server" Text="Zaloguj" 
        onclick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
            Text="Rejestracja" />
    </p>
</asp:Content>