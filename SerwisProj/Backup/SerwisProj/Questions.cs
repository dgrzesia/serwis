﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace SerwisProj
{
    public class Questions
    {
        public String question;
        public int q_id;
        public List<CusButton> buttonList;
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;

        public Questions(int id)
        {
            q_id = id;
            buttonList = new List<CusButton>();

            SqlConnection sqlConnection = new SqlConnection(this.ConnectionString);
            string sqlQuery = "SELECT Pytanie, Id_pytania FROM Questions WHERE ID_Pytania = " + id;
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection);
            sqlConnection.Open();
            SqlDataReader sqlReader = sqlCommand.ExecuteReader();
            while (sqlReader.Read())
            {
                question = sqlReader["Pytanie"].ToString();
            }
            sqlReader.Close();
            sqlConnection.Close();

            sqlQuery = "SELECT odp.Odpowiedz, odp.ID_Odpowiedzi " +
                    " FROM [Serwis].[dbo].[Odpowiedzi] odp " +
                    " LEFT JOIN [Serwis].[dbo].[Question_group] qg " +
                        " ON odp.ID_Odpowiedzi = qg.ID_Odpowiedzi " +
                    " WHERE qg.ID_Pytania = " + q_id;
            sqlCommand = new SqlCommand(sqlQuery, sqlConnection);
            sqlConnection.Open();
            sqlReader = sqlCommand.ExecuteReader();
            while (sqlReader.Read())
            {
                buttonList.Add(new CusButton(q_id, (int)sqlReader["ID_Odpowiedzi"]) { Text = sqlReader["Odpowiedz"].ToString() });
            }
            sqlReader.Close();
            sqlConnection.Close();
        }
    }
}