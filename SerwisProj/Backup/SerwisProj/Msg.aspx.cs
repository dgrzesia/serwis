﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace SerwisProj
{
    public partial class Msg : System.Web.UI.Page
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
        }   

        protected void send_btn_Click(object sender, EventArgs e)
        {
            string insertSql = "INSERT INTO [Serwis].[dbo].[Message] VALUES (\'" + msgs_box.Text + "\', CURRENT_TIMESTAMP);";           
            SqlConnection myConnection = new SqlConnection(this.ConnectionString);
            myConnection.Open();
            SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
            myCommand.ExecuteNonQuery();
            myConnection.Close();

            ContentPlaceHolder contentPlaceHolder = Page.Master.FindControl("MenuContentPlaceHolder1") as ContentPlaceHolder;
            Label msglabel = new Label();
            msglabel.Text = "Wiadomość została wysłana";
            contentPlaceHolder.Controls.Add(msglabel);
            send_btn.Visible = false;
            msgs_box.Visible = false;
            msg_label.Visible = false;
            mainPageBtn.Visible = true;
        }

        protected void mainPageBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}