﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeBehind="Login.aspx.cs" Inherits="SerwisProj.Login1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContentPlaceHolder1" Runat="Server">
    <div>
        <asp:Label ID="loginLabel" runat="server" Text="Login"></asp:Label>    
        <asp:TextBox ID="loginTextBox" runat="server"></asp:TextBox>  
    </div>
    <asp:Label ID="passwordLabel" runat="server" Text="Haslo"></asp:Label>
    <asp:TextBox ID="passwordTextBox" TextMode="password"  runat="server" style="margin-bottom: 0px"></asp:TextBox>
    <p>
        <asp:Button ID="loginBtn" runat="server" onclick="Login_Click" 
            Text="Zaloguj się" />
    </p>
    <span runat="server" id="FooSpan"></span>

</asp:Content>
