﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class DiagAdvNew : System.Web.UI.Page
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;
        
        DiagSettings diagSetting = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            diagSetting = DiagSettings.getSetting();
            
            if (diagSetting.stringList.Count() == 0)
            {
                loadQuestion(1);          
            }
            else
            {
                //loadDiag();
            }
        }

        protected void loadQuestion(int id)
        {
            SqlConnection sqlConnection = new SqlConnection(this.ConnectionString);
            string sqlQuery = "SELECT Pytanie, Id_pytania FROM Questions WHERE ID_Pytania = " + id;
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection);
            sqlConnection.Open();
            SqlDataReader sqlReader = sqlCommand.ExecuteReader();
            while (sqlReader.Read())
            {
                String name, question;
                //name = sqlReader["Pytanie"].ToString();
                //question = sqlReader["Id_pytania"].ToString();
                //diagSetting.stringList.Add(sqlReader["Pytanie"].ToString(), sqlReader["Id_pytania"].ToString());
                Label namelabel = new Label();
                namelabel.Text = sqlReader["Pytanie"].ToString();
                namelabel.ID = sqlReader["Id_pytania"].ToString();
                form1.Controls.Add(namelabel);
                //if (!diagSetting.stringList.ContainsKey(sqlReader["Pytanie"].ToString())){
                    
                //}
                loadButton(id);
            }

        }
       
        private void btn_click(object sender, EventArgs e)
        {
            ButtonAdv but = sender as ButtonAdv;
            foreach (Control btn in form1.Controls)
            {
                if (btn is ButtonAdv)
                {
                    if ((but.questionId == ((ButtonAdv)btn).questionId) && (but.flag != ((ButtonAdv)btn).flag))
                    {
                        ((ButtonAdv)btn).Enabled = true;
                        ((ButtonAdv)btn).BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                    }
                }
            }
            but.Enabled = false;
            but.BackColor = System.Drawing.Color.Red;

            foreach (ButtonAdv btn in diagSetting.buttonList)
            {
                if ((but.questionId == btn.questionId) && (but.flag == btn.flag))
                {
                    btn.Enabled = false;
                    btn.BackColor = System.Drawing.Color.Red;
                }
                else if ((but.questionId == btn.questionId) && (but.flag != btn.flag))
                {
                    btn.Enabled = true;
                    btn.BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
                }
            }
        }

        public void loadDiag()
        {
            foreach (var item in diagSetting.stringList)
            {
                Label namelabel = new Label();
                namelabel.Text = item.Key;
                namelabel.ID = item.Value;
                form1.Controls.Add(namelabel);
                form1.Controls.Add(new LiteralControl("<p></p>"));
                foreach (ButtonAdv button in diagSetting.buttonList)
                {
                    if (button.questionId == Convert.ToInt32(namelabel.ID))
                    {
                        form1.Controls.Add(button);
                    }
                }
            }
        }

        public void loadButton(int id_question)
        {
            int buttonY = 0, buttonN = 0;
            form1.Controls.Add(new LiteralControl("<p></p>"));

            SqlConnection sqlConnection = new SqlConnection(this.ConnectionString);
            string sqlQuery = "SELECT ID_Nastepne, ID_Odpowiedzi, ID_Rozwiazanie FROM Question_group WHERE ID_Pytania = " + Convert.ToString(id_question);
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection);
            sqlConnection.Open();
            SqlDataReader sqlReader = sqlCommand.ExecuteReader();
            while (sqlReader.Read())
            {
                if (sqlReader["ID_Odpowiedzi"].ToString() == "1")
                {
                    buttonY = Convert.ToInt32(sqlReader["ID_Nastepne"].ToString());
                } else if (sqlReader["ID_Odpowiedzi"].ToString() == "2")
                {
                    if (sqlReader["ID_Nastepne"].ToString() != "")
                    {
                        buttonN = Convert.ToInt32(sqlReader["ID_Nastepne"].ToString());
                    }
                }
            }

            ButtonAdv butT = new ButtonAdv(true, id_question, true, 4);
            butT.Click += new EventHandler(btn_click);
            
            form1.Controls.Add(butT);
            diagSetting.buttonList.Add(butT);
            ButtonAdv butN = new ButtonAdv(false, id_question, true, 8);
            butN.Click += new EventHandler(btn_click);
            
            form1.Controls.Add(butN);
            diagSetting.buttonList.Add(butN);

            checkClicked();
        }

        public void checkClicked()
        {
            foreach (ButtonAdv button in diagSetting.buttonList)
            {
                if (button.Enabled == false)
                {
                    loadQuestion(button.nextId);
                }
            }
        }
    }
}

    
