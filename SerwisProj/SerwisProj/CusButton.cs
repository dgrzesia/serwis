﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SerwisProj
{
    public class CusButton : System.Web.UI.WebControls.Button
    {
        private int question_id;
        private int answer_id;
        private bool pushed;
        
        public CusButton(int i, int j)
        {
            question_id = i;
            answer_id = j;
        }

        public int getQuestionId()
        {
            return question_id;
        }

        public int getAnswerId()
        {
            return answer_id;
        }

        public bool isPushed()
        {
            return pushed;
        }

        public void push(bool tmp)
        {
            pushed = tmp;
            if (tmp)
            {
                BackColor = System.Drawing.Color.Red;
            }
            else
            {
                BackColor = System.Drawing.Color.FromArgb(127, 153, 102);
            }
        }

        public bool equal(CusButton cb)
        {
            if ((this.answer_id == cb.getAnswerId()) && (this.question_id == cb.getQuestionId()))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}