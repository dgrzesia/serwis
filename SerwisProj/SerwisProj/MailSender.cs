﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;

namespace SerwisProj
{
    public class MailSender
    {
        String to;
        String name;
        public  MailSender(String to, String name)
        {
            this.to = to;
            this.name = name;
        }

        public bool send()
        {
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.UseDefaultCredentials = false;
            smtpClient.EnableSsl = true;
            smtpClient.Port = 587;
            MailMessage message = new MailMessage();
            MailAddress from = new MailAddress("super.serwis.jest.zajety@gmail.com", "Noreply Serwis");

            message.From = from;
            message.To.Add(to);
            message.Subject = "Potwierdzenie konta";
            message.Body = "Witaj "+ name + "\nAby potwierdzic konto przepisz tekst do pola tekstowego na naszej stronie \n" +
                "Tekst: KtórędyNaWałbrzych";
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Credentials = new System.Net.NetworkCredential("super.serwis.jest.zajety@gmail.com", "niewiem1");
            try
            {
                smtpClient.SendAsync(message, to);
                return true;
            }
            catch (SmtpException ex)
            {
                return false;
            }
            
        }
    }
}