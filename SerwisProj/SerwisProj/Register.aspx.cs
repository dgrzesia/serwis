﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class Register : System.Web.UI.Page
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;
        TextBox tb;
        ContentPlaceHolder cph;

        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void Button6_Click(object sender, EventArgs e)
        {
            {
                cph = Master.FindControl("MenuContentPlaceHolder1") as ContentPlaceHolder;
                if (!((TextBox2.Text == "") || (TextBox3.Text == "") || (TextBox5.Text == "") || (TextBox6.Text == "")
                    || (TextBox7.Text == "") || (TextBox8.Text == "") || (TextBox9.Text == "") || (TextBox10.Text == "") || (TextBox4.Text == "")))
                {
                    MailSender mailSender = new MailSender(TextBox7.Text, TextBox3.Text);
                    mailSender.send();

                    RegData.fname = TextBox3.Text;
                    RegData.sname = TextBox2.Text;
                    RegData.login = TextBox5.Text;
                    RegData.password = TextBox6.Text;
                    RegData.mail = TextBox7.Text;
                    RegData.street = TextBox4.Text;
                    RegData.nr = TextBox8.Text;
                    RegData.city = TextBox10.Text;
                    RegData.country = TextBox9.Text;

                    Response.Redirect("Potwierdz.aspx");                 
                }
                else
                {
                    Label label = new Label();
                    label.ID = "confirmLabel";
                    label.Text = "Wprowadź wszystkie dane!";
                    Master.FindControl("MenuContentPlaceHolder1").Controls.Add(label);
                    Master.FindControl("MenuContentPlaceHolder1").Controls.Add(new LiteralControl("<p></p>"));
                }
            }
        }

            
    }
}