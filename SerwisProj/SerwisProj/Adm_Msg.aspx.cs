﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SerwisProj
{
    public partial class Adm_Msg : System.Web.UI.Page
    {
        private int Worker_ID;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Authenticate();
            //ListView1.Scrollable = true;
        }

        protected void Messages_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Authenticate()
        {
            object state;
            if (null == (state = HttpContext.Current.Session["id_user"]))
            {
                this.ToLogin();
            }
            Worker_ID = Int32.Parse(state.ToString());
        }

        protected void ToLogin()
        {
            Response.Redirect("Default.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            ToLogin();
        }



    }
}