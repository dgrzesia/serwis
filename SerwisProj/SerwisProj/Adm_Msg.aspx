﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Adm_Msg.aspx.cs" Inherits="SerwisProj.Adm_Msg" MasterPageFile="~/Admin.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContentPlaceHolder1" Runat="Server">
    <div id="admMsg">
        <br />
<asp:GridView ID="Messages" runat="server" AutoGenerateColumns="False" 
    DataSourceID="SqlDataSource1" 
    onselectedindexchanged="Messages_SelectedIndexChanged" AllowPaging="True" 
            AllowSorting="True" Width="800">
        <Columns>
            <asp:TemplateField HeaderText="Wiadomość" SortExpression="msg" >
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("msg") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" Width="500"  runat="server" Text='<%# Bind("msg") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="400px" />
            </asp:TemplateField>
            <asp:BoundField DataField="mail" HeaderText="Adres nadawcy" 
                SortExpression="mail" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="date_in" HeaderText="Data dodania" 
                SortExpression="date_in" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
        </Columns>
    </asp:GridView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:SerwisConnectionString %>" 
    SelectCommand="SELECT [msg], [date_in], [mail] FROM [Message] ORDER BY [date_in] DESC">
</asp:SqlDataSource>
        <asp:Button ID="logOut" runat="server" Text="Wyloguj się" 
            onclick="Button1_Click" class="logOutBtn"/>
        <br />
        </div>
</asp:Content>