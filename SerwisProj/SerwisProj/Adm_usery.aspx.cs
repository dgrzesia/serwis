﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class Adm_usery : System.Web.UI.Page
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;
        public int Client_ID;

        protected void Authenticate()
        {
            object state;
            if (null == (state = HttpContext.Current.Session["id_user"]))
            {
                this.ToLogin();
            }
            Client_ID = Int32.Parse(state.ToString());
        }

        protected void ToLogin()
        {
            Response.Redirect("Default.aspx");
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Authenticate();


            using (SqlConnection sqlConnection = new SqlConnection(this.ConnectionString))
            {
                string sqlQuery = @"SELECT  u.email as uNazwa, u.nazwisko as uNazwisko, u.imie as uImie,
		                                u.login as uLogin, u.haslo as uHaslo, u.id_typ as uIDT, u.id_user as uIDU,
		                                a.Ulica as aUlica, a.Nr as aNr, a.Miasto as aMiasto, a.Państwo as aPanstwo,
                                        t.stanowisko as tStanowisko 
                                from usery as u
                                left join adres as a on a.id_u = u.id_user
                                left join id_typ as t on t.id_typ= u.id_typ
                                order by tStanowisko, uNazwa, uNazwisko";
                using (SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection))
                {
                    sqlConnection.Open();
                    using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
                    {
                        Table1.Width = 960;
                        using (TableRow tableRow = new TableRow())
                        {
                            Table1.Rows.Add(tableRow);
                            tableRow.Cells.Add(new TableCell() { ColumnSpan = 13, VerticalAlign = VerticalAlign.Middle, HorizontalAlign = HorizontalAlign.Center });
                        }
                        while (sqlReader.Read())
                        {


                            using (TableRow tableRow = new TableRow())
                            {
                                {
                                    Table1.Rows.Add(tableRow);
                                    tableRow.Cells.Add(new TableCell() { Text = sqlReader["uNazwa"].ToString() });
                                    tableRow.Cells.Add(new TableCell() { Text = sqlReader["uNazwisko"].ToString() });
                                    tableRow.Cells.Add(new TableCell() { Text = sqlReader["uImie"].ToString() });
                                    tableRow.Cells.Add(new TableCell() { Text = sqlReader["aUlica"].ToString() });
                                    tableRow.Cells.Add(new TableCell() { Text = sqlReader["aNr"].ToString() });
                                    tableRow.Cells.Add(new TableCell() { Text = sqlReader["aMiasto"].ToString() });
                                    tableRow.Cells.Add(new TableCell() { Text = sqlReader["aPanstwo"].ToString() });
                                    tableRow.Cells.Add(new TableCell() { Text = sqlReader["uLogin"].ToString() });
                                    tableRow.Cells.Add(new TableCell() { Text = sqlReader["tStanowisko"].ToString() });

                                    Button editButton = new Button { Text = "Edytuj", ID = "1" + sqlReader["uIDU"].ToString() };
                                    editButton.CssClass = "zamBtn";
                                    editButton.Click += new EventHandler(edit_click);
                                    Button removeButton = new Button { Text = "Usuń", ID = "2" + sqlReader["uIDU"].ToString() };
                                    removeButton.CssClass = "zamBtn";
                                    removeButton.Click += new EventHandler(delete_click);

                                    TableCell tc = new TableCell();
                                    tc.Controls.Add(editButton);
                                    tc.Controls.Add(removeButton);
                                    tableRow.Cells.Add(tc);

                                }
                            }
                        }
                        sqlReader.Close();
                    }

                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            ToLogin();
        }


        protected void Button6_Click(object sender, EventArgs e)
        {
            {
                string insertSql = "insert into usery(nazwa, imie, nazwisko, login, haslo, id_typ, email) OUTPUT (inserted.id_user) into adres(id_u) values('-', @imie, @nazwisko, @login, @haslo,@id_typ, @email)";

                using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
                {
                    myConnection.Open();

                    SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                    myCommand.Parameters.AddWithValue("@email", TextBox7.Text);
                    myCommand.Parameters.AddWithValue("@imie", TextBox3.Text);
                    myCommand.Parameters.AddWithValue("@nazwisko", TextBox2.Text);
                    myCommand.Parameters.AddWithValue("@login", TextBox5.Text);
                    myCommand.Parameters.AddWithValue("@haslo", TextBox6.Text);
                    myCommand.Parameters.AddWithValue("@id_typ", TextBox7.Text);

                    myCommand.ExecuteNonQuery();
                    myConnection.Close();

                }

                string insertSql2 = @"UPDATE adres SET ulica = @ulica, nr=@nr, 
                                            miasto = @miasto, państwo = @panstwo
                                  WHERE id_u=(select MAX(id_user) from usery)";

                using (SqlConnection myConnection2 = new SqlConnection(this.ConnectionString))
                {
                    myConnection2.Open();

                    SqlCommand myCommand2 = new SqlCommand(insertSql2, myConnection2);
                    myCommand2.Parameters.AddWithValue("@ulica", TextBox4.Text);
                    myCommand2.Parameters.AddWithValue("@nr", TextBox8.Text);
                    myCommand2.Parameters.AddWithValue("@miasto", TextBox10.Text);
                    myCommand2.Parameters.AddWithValue("@panstwo", TextBox9.Text);
                    //myCommand2.Parameters.AddWithValue("@id_user", );
                    //ParameterDirection.Output

                    myCommand2.ExecuteNonQuery();
                    myConnection2.Close();

                }
                Response.Redirect(Request.RawUrl);
            }
        }
        protected void Button5_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            ToLogin();
        }

        public void edit_click(object sender, EventArgs e)
        {
            int ID = Int16.Parse(((Button)sender).ID);
            if (ID > 1000)
            {
                ID -= 1000;
            }
            else if (ID > 100)
            {
                ID -= 100;
            }
            else
                ID -= 10;

            RegData.editedId = ID;
            Response.Redirect("User_Edit.aspx");

        }

        public void delete_click(object sender, EventArgs e)
        {
            int ID = Int16.Parse(((Button)sender).ID);
            if (ID > 2000){
                ID -= 2000;
            }
            else if (ID > 200)
            {
                ID -= 200;
            }
            else
                ID -=20;
            
            string insertSql = "DELETE FROM usery WHERE id_user=" + ID.ToString();

            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();

                SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                object sklep = myCommand.ExecuteScalar();

                myCommand.ExecuteNonQuery();
                myConnection.Close();

            }
            Response.Redirect(Request.RawUrl);
        }
    }
}