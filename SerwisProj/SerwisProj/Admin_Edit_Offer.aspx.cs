﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class Admin_Edit_Offer : System.Web.UI.Page
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (SqlConnection sqlConnection = new SqlConnection(this.ConnectionString))
                {
                    string sqlQuery = @"select id_uslugi ,u.Nazwa as uNazwa, u.cena as ucena from Uslugi as u WHERE ID_uslugi =" + RegData.editedId;
                    using (SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection))
                    {
                        sqlConnection.Open();
                        using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                TextBox1.Text = sqlReader["uNazwa"].ToString();
                                TextBox2.Text = sqlReader["ucena"].ToString();
                            }
                            sqlReader.Close();
                        }
                    }
                }
            }
        }

        public void btn_click(object sender, EventArgs e)
        {
            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();

                String insertSql = @"UPDATE Uslugi SET Nazwa = '" + TextBox1.Text + "', cena = " + TextBox2.Text + " WHERE ID_uslugi =" + RegData.editedId;
                SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                myCommand = new SqlCommand(insertSql, myConnection);
                myCommand.ExecuteNonQuery();
                myConnection.Close();
            }
            Response.Redirect("Adm_produkt.aspx");
        }

    }
}