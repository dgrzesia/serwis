﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeBehind="Diag.aspx.cs" Inherits="SerwisProj.Diag" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContentPlaceHolder1" Runat="Server">
    <div id="yesno">
    <p><label>Komputer sie włącza?</label>
        <br />
        <br />
        <asp:Button ID="TurnOnTrue" runat="server" AutoPostBack="true" Text="Tak" onclick="TurnOnTrue_Click" 
            />
        <asp:Button ID="TurnOnFalse" runat="server" AutoPostBack="true" Text="Nie" onclick="TurnOnFalse_Click" 
            />
    </p>
    <p>

        <label><asp:Label ID="Label1" runat="server"></asp:Label></label>
        <br />
        <br />
        <asp:Button ID="VisibleTrue" runat="server" Visible="False" AutoPostBack="true" 
            Text="Tak" onclick="VisibleTrue_Click" />
        <asp:Button ID="VisibleFalse" runat="server" Visible="False" AutoPostBack="true" 
            Text="Nie" onclick="VisibleFalse_Click" />
    </p>
    <p>
        
         <label><asp:Label ID="Label2" runat="server" Visible="False" Text="Label"></asp:Label> </label>
        <asp:Panel ID="BipPanel" runat="server" Visible="False">
            <asp:Button ID="BipTrue" runat="server" Visible="True"  AutoPostBack="true" onclick="BipTrue_Click"
                 />
            <asp:Button ID="BipFalse" runat="server" Visible="True"  AutoPostBack="true" onclick="BipFalse_Click"
                 />     
        </asp:Panel>
        <asp:Panel ID="BiosMsg" runat="server" Visible="False">
            <asp:Button ID="Disc" runat="server" Visible="True" Text="Disk error occured" AutoPostBack="true" onclick="Disc_Click"
                     />
            <asp:Button ID="NTLDR" runat="server" Visible="True" Text="Brak pliku NTLDR" AutoPostBack="true" onclick="NTLDR_Click"
                     />
            <asp:Button ID="F1" runat="server" Visible="True" Text="Press F1 to continue" AutoPostBack="true" onclick="F1_Click"
                     />
            <asp:Button ID="Zaden" runat="server" Visible="True" Text="Brak komunikatu" AutoPostBack="true" onclick="Zaden_Click"
                     />
        </asp:Panel>    
    </p>
    <p>
        
        <asp:Label ID="Label3" runat="server" Text="Label" Visible="False"></asp:Label>

        <asp:Panel ID="SpeakerBlack" runat="server" Visible="False">
            <asp:Button ID="SpeakerBlackTrue" runat="server"  AutoPostBack="true" Text = "Tak" onclick="SpeakerBlackTrue_Click"
                />
            <asp:Button ID="SpeakerBlackFalse" runat="server"  AutoPostBack="true" Text="Nie" onclick="SpeakerBlackFalse_Click"
                 />     
        </asp:Panel>
        <br />
        <asp:Label ID="Label5" runat="server" Text="Label" Visible="False"></asp:Label>
        <br />
        <br />
        <asp:Panel ID="SystemLoadingPanel" runat="server" Visible="False">
            <asp:Button ID="SystemLoadingTrue" runat="server"  AutoPostBack="true" onclick="SystemLoadingTrue_Click"
                />
            <asp:Button ID="SystemLoadingFalse" runat="server"  AutoPostBack="true" onclick="SystemLoadingFalse_Click"
                 />     
        </asp:Panel>
    </p>
    <p>
        <asp:Label ID="Label4" runat="server" Text="Label" Visible="False"></asp:Label>
        
        <asp:Panel ID="SystemLoading" runat="server" Visible="False">
            <asp:Button ID="SystemLoaded" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="SystemLoaded_Click" />
            <asp:Button ID="SystemNotLoaded" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="SystemNotLoaded_Click"/>
        </asp:Panel>
    </p>
    <p>
        <asp:Label ID="Label6" runat="server" Text="Label" Visible="False"></asp:Label>
        
        <asp:Panel ID="WithoutError" runat="server" Visible="False">
            <asp:Button ID="woErrorT" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="woErrorT_Click" />
            <asp:Button ID="woErrorF" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="woErrorF_Click"/>
        </asp:Panel>    
    </p>
    <p>
        <asp:Label ID="Label7" runat="server" Text="Label" Visible="False"></asp:Label>
        
        <asp:Panel ID="ResetedPanel" runat="server" Visible="False">
            <asp:Button ID="ResetedT" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="ResetedT_Click" />
            <asp:Button ID="ResetedF" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="ResetedF_Click"/>
        </asp:Panel>
        <asp:Panel ID="LoginErrorPanel" runat="server" Visible="False">
            <asp:Button ID="LoginError" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="LoginError_Click" />
            <asp:Button ID="LoginNotError" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="LoginNotError_Click"/>
        </asp:Panel>
    </p>
    <p>
        <asp:Label ID="Label8" runat="server" Text="Label" Visible="False"></asp:Label>
        
        <asp:Panel ID="PasswdPanel" runat="server" Visible="False">
            <asp:Button ID="PassOk" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="PassOk_Click" />
            <asp:Button ID="PassF" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="PassF_Click"/>
        </asp:Panel>
        <asp:Panel ID="DesktopPanel" runat="server" Visible="False">
            <asp:Button ID="DesktopT" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="DesktopT_Click" />
            <asp:Button ID="DeskktopF" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="DeskktopF_Click"/>
        </asp:Panel>
    </p>
    <p>    
        <asp:Label ID="Label10" runat="server" Text="Label" Visible="False"></asp:Label>
           
        <asp:Panel ID="LoadUserPanel" runat="server" Visible="False">
            <asp:Button ID="LoadT" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="LoadT_Click" />
            <asp:Button ID="LoadF" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="LoadF_Click"/>
        </asp:Panel> 
    </p>
    <p>
        <asp:Label ID="Label9" runat="server" Text="Label" Visible="False"></asp:Label>
        <asp:Panel ID="DesktopPanel2" runat="server" Visible="False">
            <asp:Button ID="DesktopT2" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="DesktopT2_Click" />
            <asp:Button ID="DesktopF3" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="DeskktopF2_Click"/>
        </asp:Panel>
    </p>

    <p>
        <asp:Label ID="Label11" runat="server" Text="Label" Visible="False"></asp:Label>
        <asp:Panel ID="BlackPx" runat="server" Visible="False">
            <asp:Button ID="BlackPxT" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="BlackPxT_Click" />
            <asp:Button ID="BlackPxF" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="BlackPxF_Click"/>
        </asp:Panel>
        <asp:Panel ID="Resolution" runat="server" Visible="False">
            <asp:Button ID="ResYes" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="ResYes_Click" />
            <asp:Button ID="ResNo" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="ResNo_Click"/>
        </asp:Panel>
    </p>
    <p>
        <asp:Label ID="Label12" runat="server" Text="Label" Visible="False"></asp:Label> 

        <asp:Panel ID="MultiBlurPanel" runat="server" Visible="False">
            <asp:Button ID="MultiY" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="MultiY_Click" />
            <asp:Button ID="MultiN" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="MultiN_Click"/>
        </asp:Panel>
        <asp:Panel ID="UpdatePanel" runat="server" Visible="False">
            <asp:Button ID="UpdateY" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="UpdateY_Click" />
            <asp:Button ID="UpdateN" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="UpdateN_Click"/>
        </asp:Panel>
    </p>
    <p>
        <asp:Label ID="Label13" runat="server" Text="Label" Visible="False"></asp:Label>
        <asp:Panel ID="HowTimePanel" runat="server" Visible="False">
            <asp:Button ID="TimeY" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="TimeY_Click" />
            <asp:Button ID="TimeN" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="TimeN_Click"/>
        </asp:Panel>
        <asp:Panel ID="AfterErrorPanel" runat="server" Visible="False">
            <asp:Button ID="AfterY" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="AfterY_Click" />
            <asp:Button ID="AfterN" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="AfterN_Click"/>
        </asp:Panel>
    </p>
    <p>
        <asp:Label ID="Label14" runat="server" Text="Label" Visible="False"></asp:Label>
        
        <asp:Panel ID="BsodPanel" runat="server" Visible="False">
            <asp:Button ID="BsodY" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="BsodY_Click" />
            <asp:Button ID="BsodN" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="BsodN_Click"/>
        </asp:Panel>    
    </p>
    <p>
       <asp:Label ID="Label15" runat="server" Text="Label" Visible="False"></asp:Label>
       <asp:Panel ID="ApplicationPanel" runat="server" Visible="False">
            <asp:Button ID="AppY" runat="server" Text="Tak"  
                AutoPostBack="true" onclick="AppY_Click" />
            <asp:Button ID="AppN" runat="server"  Text="Nie"
                AutoPostBack="true" onclick="AppN_Click"/>
        </asp:Panel>
        
        <asp:Panel ID="BsodoNrPanel" runat="server" Visible="False">
            
            <asp:DropDownList ID="BsodList" runat="server" AutoPostBack="True" 
                DataSourceID="SqlDataSource1" DataTextField="bsod_nr" DataValueField="id" 
                onselectedindexchanged="BsodList_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ServicesConnectionString %>" 
                SelectCommand="SELECT [id], [bsod_nr] FROM [bsod]"></asp:SqlDataSource>
            
        </asp:Panel>   
    </p>
    <p>
        <asp:Label ID="Label16" runat="server" Text="Label" Visible="False"></asp:Label>
       
    </p>
    </div>
    
</asp:Content>
