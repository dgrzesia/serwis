﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;

namespace SerwisProj
{
    public partial class send_mail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TextBox2.Text = RegData.mail;
            }
        }

        protected void btn_Click(object sender, EventArgs e)
        {
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.UseDefaultCredentials = false;
            smtpClient.EnableSsl = true;
            smtpClient.Port = 587;
            MailMessage message = new MailMessage();
            MailAddress from = new MailAddress("super.serwis.jest.zajety@gmail.com", "Serwis Komputerowy");

            message.From = from;
            message.To.Add(TextBox2.Text);
            message.Subject = TextBox3.Text;
            message.Body = TextBox1.Text;
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Credentials = new System.Net.NetworkCredential("super.serwis.jest.zajety@gmail.com", "niewiem1");
            try
            {
                smtpClient.SendAsync(message, TextBox2.Text);
            }
            catch (SmtpException ex)
            {
                RegData.mail="";
                Response.Redirect(Request.RawUrl);
            }
            RegData.mail = "";
            Response.Redirect(Request.RawUrl);
        }
    }
}