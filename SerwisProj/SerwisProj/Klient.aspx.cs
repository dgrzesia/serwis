﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class Klient : System.Web.UI.Page
    {

        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;
        public int Client_ID;

        protected void Authenticate()
        {
            object state;
            if (null == (state = HttpContext.Current.Session["id_user"]))
            {
                this.ToLogin();
            }
            Client_ID = Int32.Parse(state.ToString());
        }

        protected void ToLogin()
        {
            Response.Redirect("Default.aspx");
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Authenticate();
            using (SqlConnection sqlConnection = new SqlConnection(this.ConnectionString))
            {
                string sqlQuery = @"select id_uslugi ,u.Nazwa as uNazwa, u.cena as ucena from Uslugi as u";
                using (SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection))
                {
                    sqlConnection.Open();
                    using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
                    {
                        Table1.Width = 960;
                        using (TableRow tableRow = new TableRow())
                        {
                            Table1.Rows.Add(tableRow);
                            tableRow.Cells.Add(new TableCell() { ColumnSpan = 3, VerticalAlign = VerticalAlign.Middle, HorizontalAlign = HorizontalAlign.Center });
                        }
                        while (sqlReader.Read())
                        {
                            using (TableRow tableRow = new TableRow())
                            {
                                Table1.Rows.Add(tableRow);
                                tableRow.Cells.Add(new TableCell() { Text = sqlReader["uNazwa"].ToString() });
                                tableRow.Cells.Add(new TableCell() { Text = sqlReader["ucena"].ToString() + " zł" });
                                Button buyButton = new Button { Text = "Zamow", ID = sqlReader["id_uslugi"].ToString() };
                                buyButton.CssClass = "zamBtn";
                                // TextBox textBox = new TextBox { };
                                buyButton.Click += new EventHandler(btn_click);

                                TableCell tc = new TableCell();
                                tc.Controls.Add(buyButton);
                                tableRow.Cells.Add(tc);
                            }
                        }
                        sqlReader.Close();
                    }
                }
            }
        }




        public void btn_click(object sender, EventArgs e)
        {
            int ID = Int16.Parse(((Button)sender).ID);


            string insertSql = "select id_user from usery where id_user = @id_user";

            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();

                SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                myCommand.Parameters.AddWithValue("@id_user", this.Client_ID);
                object user = myCommand.ExecuteScalar();

                insertSql = "insert into zamowienia(id_uslugi, id_user, data) values(@id_uslugi, @id_user, CURRENT_TIMESTAMP)";
                myCommand = new SqlCommand(insertSql, myConnection);
                myCommand.Parameters.AddWithValue("@id_uslugi", ID);
                myCommand.Parameters.AddWithValue("@id_user", user);
                myCommand.ExecuteNonQuery();
                myConnection.Close();

            }
            RegData.fromNewOrder = true;
            Response.Redirect("Klient_zam.aspx");

        }

        public override void VerifyRenderingInServerForm(Control control) { }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            ToLogin();
        }
    }
}