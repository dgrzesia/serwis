﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class Potwierdz : System.Web.UI.Page
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void confirmBtn(object sender, EventArgs e)
        {
            if (confirmText.Text == "KtórędyNaWałbrzych")
            {
                string insertSql = "insert into usery(nazwa, imie, nazwisko, login, haslo, id_typ, email) OUTPUT (inserted.id_user) into adres(id_u) values(@nazwa, @imie, @nazwisko, @login, @haslo,@id_typ, @email)";

                using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
                {
                    myConnection.Open();

                    SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                    myCommand.Parameters.AddWithValue("@nazwa", "-");
                    myCommand.Parameters.AddWithValue("@imie", RegData.fname);
                    myCommand.Parameters.AddWithValue("@nazwisko", RegData.sname);
                    myCommand.Parameters.AddWithValue("@login", RegData.login);
                    myCommand.Parameters.AddWithValue("@haslo", RegData.password);
                    myCommand.Parameters.AddWithValue("@id_typ", "1");
                    myCommand.Parameters.AddWithValue("@email", RegData.mail);

                    myCommand.ExecuteNonQuery();
                    myConnection.Close();

                }

                string insertSql2 = @"UPDATE adres SET ulica = @ulica, nr=@nr, 
                                                    miasto = @miasto, państwo = @panstwo
                                          WHERE id_u=(select MAX(id_user) from usery)";

                using (SqlConnection myConnection2 = new SqlConnection(this.ConnectionString))
                {
                    myConnection2.Open();

                    SqlCommand myCommand2 = new SqlCommand(insertSql2, myConnection2);
                    myCommand2.Parameters.AddWithValue("@ulica", RegData.street);
                    myCommand2.Parameters.AddWithValue("@nr", RegData.nr);
                    myCommand2.Parameters.AddWithValue("@miasto", RegData.city);
                    myCommand2.Parameters.AddWithValue("@panstwo", RegData.country);

                    myCommand2.ExecuteNonQuery();
                    myConnection2.Close();

                }
                String page = "";
                page = "Default.aspx";
                Response.Redirect(page);
            }
            else
            {
                Label label = new Label();
                label.ID = "errorLabel";
                label.Text = "Niepoprawny tekst";
                Master.FindControl("MenuContentPlaceHolder1").Controls.Add(label);
                Master.FindControl("MenuContentPlaceHolder1").Controls.Add(new LiteralControl("<p></p>"));
            }
        }
    }
}