﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class Adm_produkty : System.Web.UI.Page
    {
        private int Worker_ID;
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;

        protected void Authenticate()
        {
            object state;
            if (null == (state = HttpContext.Current.Session["id_user"]))
            {
                this.ToLogin();
            }
            Worker_ID = Int32.Parse(state.ToString());
        }

        protected void ToLogin()
        {
            Response.Redirect("Default.aspx");
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Authenticate();
            using (SqlConnection sqlConnection = new SqlConnection(this.ConnectionString))
            {
                string sqlQuery = @"select id_uslugi ,u.Nazwa as uNazwa, u.cena as ucena from Uslugi as u";
                using (SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection))
                {
                    sqlConnection.Open();
                    using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
                    {
                        offerTable.Width = 960;
                        using (TableRow tableRow = new TableRow())
                        {
                            offerTable.Rows.Add(tableRow);
                            tableRow.Cells.Add(new TableCell() { ColumnSpan = 4, VerticalAlign = VerticalAlign.Middle, HorizontalAlign = HorizontalAlign.Center });
                        }
                        while (sqlReader.Read())
                        {
                            using (TableRow tableRow = new TableRow())
                            {
                                offerTable.Rows.Add(tableRow);
                                tableRow.Cells.Add(new TableCell() { Text = sqlReader["uNazwa"].ToString() });
                                tableRow.Cells.Add(new TableCell() { Text = sqlReader["ucena"].ToString() + " zł" });
                                Button editButton = new Button { Text = "Edytuj", ID = "1" + sqlReader["id_uslugi"].ToString() };
                                editButton.CssClass = "zamBtn";
                                editButton.Click += new EventHandler(edit_click);
                                Button removeButton = new Button { Text = "Usuń", ID = "2" + sqlReader["id_uslugi"].ToString()};
                                removeButton.CssClass = "zamBtn";
                                removeButton.Click += new EventHandler(delete_click);

                                TableCell tc = new TableCell();
                                tc.Controls.Add(editButton);
                                tc.Controls.Add(removeButton);
                                tableRow.Cells.Add(tc);
                            }
                        }
                        sqlReader.Close();
                    }
                }
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            ToLogin();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string insertSql = "insert into Uslugi(Nazwa, cena) values(@nazwa, @cena)";

            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();

                SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                myCommand.Parameters.AddWithValue("@nazwa", TextBox1.Text);
                //myCommand.Parameters.AddWithValue("@ID_P", DropDownList1.SelectedValue);
                myCommand.Parameters.AddWithValue("@cena", TextBox3.Text);
                myCommand.ExecuteNonQuery();
                myConnection.Close();

            }
            Response.Redirect(Request.RawUrl);
        }
        protected void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void edit_click(object sender, EventArgs e)
        {
            int ID = Int16.Parse(((Button)sender).ID);
            if (ID > 1000)
            {
                ID -= 1000;
            }
            else if (ID > 100)
            {
                ID -= 100;
            }
            else
                ID -= 10;

            RegData.editedId = ID;
            Response.Redirect("Admin_Edit_Offer.aspx");

        }

        public void delete_click(object sender, EventArgs e)
        {
            int ID = Int16.Parse(((Button)sender).ID);
            if (ID > 2000)
            {
                ID -= 2000;
            }
            else if (ID > 200)
            {
                ID -= 200;
            }
            else
                ID -= 20;
            string insertSql = "DELETE FROM Uslugi WHERE ID_uslugi=" + ID.ToString();

            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();

                SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                object sklep = myCommand.ExecuteScalar();

                myCommand.ExecuteNonQuery();
                myConnection.Close();

            }
            Response.Redirect(Request.RawUrl);
        }
    }
}