﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SerwisProj
{
    public partial class User_Edit : System.Web.UI.Page
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (SqlConnection sqlConnection = new SqlConnection(this.ConnectionString))
                {
                    string sqlQuery = @"SELECT  u.email as uMail, u.nazwisko as uNazwisko, u.imie as uImie,
		                                u.login as uLogin, u.haslo as uHaslo, u.id_typ as uIDT, u.id_user as uIDU,
		                                a.Ulica as aUlica, a.Nr as aNr, a.Miasto as aMiasto, a.Państwo as aPanstwo,
                                        t.stanowisko as tStanowisko 
                                from usery as u
                                left join adres as a on a.id_u = u.id_user
                                left join id_typ as t on t.id_typ= u.id_typ
                                WHERE u.id_user =" + RegData.editedId;
                    using (SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection))
                    {
                        sqlConnection.Open();
                        using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                TextBox5.Text = sqlReader["uLogin"].ToString();
                                //TextBox6.Text = sqlReader["uHaslo"].ToString();
                                TextBox7.Text = sqlReader["uMail"].ToString();
                                TextBox2.Text = sqlReader["uNazwisko"].ToString();
                                TextBox3.Text = sqlReader["uImie"].ToString();
                                TextBox4.Text = sqlReader["aUlica"].ToString();
                                TextBox8.Text = sqlReader["aNr"].ToString();
                                TextBox10.Text = sqlReader["aMiasto"].ToString();
                                TextBox9.Text = sqlReader["aPanstwo"].ToString();
                                TextBox1.Text = sqlReader["uIDT"].ToString();
                            }
                            sqlReader.Close();
                        }
                    }
                }
            }
        }

        public void btn_click(object sender, EventArgs e)
        {
            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();

                if (TextBox6.Text != "")
                {
                    String insertSql = @"UPDATE usery SET Nazwa = '-', imie = '" + TextBox3.Text + "', nazwisko = '" +
                        TextBox2.Text + "', login = '" + TextBox5.Text + "', haslo = '" + TextBox6.Text + "', id_typ = " +
                        TextBox1.Text + ", email = '" + TextBox7.Text + "' WHERE id_user =" + RegData.editedId;
                    SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                    myCommand = new SqlCommand(insertSql, myConnection);
                    myCommand.ExecuteNonQuery();
                    myConnection.Close();
                }
                else
                {
                    String insertSql = @"UPDATE usery SET Nazwa = '-', imie = '" + TextBox3.Text + "', nazwisko = '" +
                        TextBox2.Text + "', login = '" + TextBox5.Text +  "', id_typ = '" +
                        TextBox1.Text + "', email = '" + TextBox7.Text + "' WHERE id_user =" + RegData.editedId;
                    SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                    myCommand = new SqlCommand(insertSql, myConnection);
                    myCommand.ExecuteNonQuery();
                    myConnection.Close();
                }
            }

            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();

                String insertSql = @"UPDATE Adres SET Ulica = '" + TextBox4.Text + "', Nr = '" +
                    TextBox8.Text + "', Miasto = '" + TextBox10.Text + "', Państwo = '" + TextBox9.Text +
                    "' Where ID_U = " + RegData.editedId;
                SqlCommand myCommand = new SqlCommand(insertSql, myConnection);
                myCommand = new SqlCommand(insertSql, myConnection);
                myCommand.ExecuteNonQuery();
                myConnection.Close();
            }
            Response.Redirect("Adm_produkt.aspx");
        }
    }
}