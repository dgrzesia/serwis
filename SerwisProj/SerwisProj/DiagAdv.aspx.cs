﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


namespace SerwisProj
{
    public static class Globals
    {
        public static List<Questions> drzewo;
        public static ContentPlaceHolder cph;
    }

    public partial class DiagAdv : System.Web.UI.Page
    {
        public string ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SerwisConnectionString"].ConnectionString;
        public ContentPlaceHolder cph;
        public ContentPlaceHolder contentPlaceHolder;
        public bool addButton;

        protected void Page_Load(object sender, EventArgs e)
        {            
            /*if (!IsPostBack)
            {
                Globals.contentPlaceHolder = Page.Master.FindControl("MenuContentPlaceHolder1") as ContentPlaceHolder;
                Globals.drzewo = new List<Questions>();
                Globals.drzewo.Add(new Questions(1));
            }
            cph = Globals.contentPlaceHolder;
            //this.MasterPageFile = "~/MyMaster.master";
            startDiag();*/

            ///Page.content = Globals.contentPlaceHolder;
                contentPlaceHolder = Page.Master.FindControl("MenuContentPlaceHolder1") as ContentPlaceHolder;
                startDiag();      
        }

        protected void startDiag()
        {
            addQuestion(1); 
            /*foreach (Questions question in Globals.drzewo)
            {
                Label namelabel = new Label();
                namelabel.Text = question.question;
                Globals.contentPlaceHolder.Controls.Add(namelabel);
                cph.Controls.Add(namelabel);

                foreach (CusButton button in question.buttonList)
                {
                    CusButton but = new CusButton(button.getQuestionId(), button.getAnswerId());
                    //Button but = new Button();
                    but.Text = button.Text;
                    but.Click += new EventHandler(btn_click);
                    Globals.contentPlaceHolder.Controls.Add(but);
                    cph.Controls.Add(but); 
                }
            }*/
        }

        protected void addQuestion(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.ConnectionString))
            {
                string sqlQuery = "SELECT Pytanie, Id_pytania FROM Questions WHERE ID_Pytania = " + id;
                using (SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection))
                {
                    sqlConnection.Open();
                    using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
                    {
                        while (sqlReader.Read())
                        {
                            Label namelabel = new Label();                            
                            namelabel.Text = sqlReader["Pytanie"].ToString();
                            namelabel.ID = sqlReader["Id_pytania"].ToString(); 
                            contentPlaceHolder.Controls.Add(namelabel);

                            SqlConnection sqlConnectionAns = new SqlConnection(this.ConnectionString);
                            string sqlAnswer = "SELECT odp.Odpowiedz, odp.ID_Odpowiedzi " +
                                    " FROM [Serwis].[dbo].[Odpowiedzi] odp " +
                                    " LEFT JOIN [Serwis].[dbo].[Question_group] qg " +
	                                    " ON odp.ID_Odpowiedzi = qg.ID_Odpowiedzi " +
                                    " WHERE qg.ID_Pytania = " + id;
                            SqlCommand sqlCommandAns = new SqlCommand(sqlAnswer, sqlConnectionAns);
                            sqlConnectionAns.Open();
                            SqlDataReader sqlReaderAns = sqlCommandAns.ExecuteReader();
                            while (sqlReaderAns.Read())
                            {
                                foreach (var control in contentPlaceHolder.Controls)
                                {
                                    var CusButton = control as CusButton;
                                    if ((CusButton != null) && !(CusButton.getAnswerId() == (int)sqlReaderAns["ID_Odpowiedzi"]) && !(CusButton.getQuestionId() == (int)sqlReader["Id_pytania"]))
                                    {
                                        addButton = false;
                                    }
                                }

                                if (addButton)
                                {
                                    CusButton but = new CusButton((int)sqlReader["Id_pytania"], (int)sqlReaderAns["ID_Odpowiedzi"]) { Text = sqlReaderAns["Odpowiedz"].ToString() };
                                    but.Click += new EventHandler(btn_click);
                                    contentPlaceHolder.Controls.Add(but);                                    
                                }
                                addButton = true;
                            }
                            sqlReaderAns.Close();
                            sqlConnectionAns.Close();
                            contentPlaceHolder.Controls.Add(new LiteralControl("<p></p>"));
                        }
                        sqlReader.Close();

                    }
                    sqlConnection.Close();
                }
            }
            //Response.Redirect(Request.RawUrl);
        }

        public void btn_click(object sender, EventArgs e)
        {
            //throw new Exception("Button Click Event Triggered.  Hello yellow screen!!!");

            CusButton button = sender as CusButton;            
            
            foreach (var control in contentPlaceHolder.Controls)
            {
                var CusButton = control as CusButton;
                if ((CusButton != null) && (button.isPushed()) && (CusButton.getQuestionId() == button.getQuestionId()))
                {
                    CusButton.push(false);
                }
            }
            button.push(true);

            string selectSql = "SELECT id_nastepne FROM [Serwis].[dbo].[Question_group] WHERE id_pytania = " + button.getQuestionId() + " AND id_odpowiedzi = " + button.getAnswerId();
            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();
                SqlCommand myCommand = new SqlCommand(selectSql, myConnection);
                SqlDataReader sqlReader = myCommand.ExecuteReader();
                while (sqlReader.Read())
                {
                    if (sqlReader["id_nastepne"].ToString() != "")
                    {
                        //addQuestion((int)sqlReader["id_nastepne"]);
                        Globals.drzewo.Add(new Questions((int)sqlReader["id_nastepne"]));
                    }   
                }
                sqlReader.Close();
                myConnection.Close();
            }

            selectSql = "SELECT Rozwiazanie " +
                    "FROM [Serwis].[dbo].[Rozwiazania] answ " +
                    "LEFT JOIN [Serwis].[dbo].[Question_group] qg " +
                    "	ON answ.ID_Rozwiazanie = qg.ID_Rozwiazanie " +
                    "WHERE qg.ID_Pytania = " + button.getQuestionId() +
                    "	AND qg.ID_Odpowiedzi = " + button.getAnswerId();
            using (SqlConnection myConnection = new SqlConnection(this.ConnectionString))
            {
                myConnection.Open();
                SqlCommand myCommand = new SqlCommand(selectSql, myConnection);
                SqlDataReader sqlReader = myCommand.ExecuteReader();
                while (sqlReader.Read())
                {
                    if (sqlReader["Rozwiazanie"].ToString() != "")
                    {
                        addAnswer(sqlReader["Rozwiazanie"].ToString());
                    }
                }
                sqlReader.Close();
                myConnection.Close();
            }
        }

        public void addAnswer(string ans)
        {
            Label namelabel = new Label();
            namelabel.Text = ans;
            contentPlaceHolder.Controls.Add(namelabel);
        }
    }
}